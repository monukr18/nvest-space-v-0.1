import React, { Component } from 'react';
import {BrowserRouter as Router,Route} from 'react-router-dom';
import HomePage from './components/Home/home';
import TechCampus from './components/TechCampus/TechCampus'
export default class Application extends Component {
    render() {
        return (
            <div>
                <Router>
		            <Route path="/" exact component={HomePage}/>
		            <Route path="/techcampus" exact component={TechCampus}/>
                </Router>
            </div>
        )
    }
}
