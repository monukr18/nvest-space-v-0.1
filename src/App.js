import React from 'react';
import './App.css';
import Navbar from './components/navbar/navbar';
import Footer from './components/Footer/footer';
import Application from './Application'
function App() {
  return (
    <div className="App">
      <Navbar />
      <Application />
      <Footer />
    </div>
  );
}

export default App;
