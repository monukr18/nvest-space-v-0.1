import React from 'react';
import './navbar.css'
import Nvestlogo from '../../images/nvestlogo.png';
import MenuButton from '../../images/menu.svg'
import {Link} from "react-router-dom";
export default function navbar() {
    return (
        <div >
<nav class=" navbar sticky-top navbar-expand-lg navbar-light " style={{height:"100px"}}>

      <img style={{width:"15%",paddingRight:"25px"}} src={Nvestlogo}  />

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto" style={{marginLeft:"100px"}}>
      <div class="nav-item active dropdown">
        <a class="nav-link" href="/techcampus">

        <h5 className="nav-txt-color nav-item-style">Tech Campus</h5>
        </a>
		</div>
      
      <div className="dropdown-content ">
			<h5>Europe - London</h5>
			
		</div>
      

      <li class="nav-item">
        <a class="nav-link" href="#">
        <h5 className="nav-txt-color nav-item-style">Startups</h5>
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link " href="#" id="" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <h5 className="nav-txt-color nav-item-style">Corporate Innovation</h5>
        </a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
          <img style={{width:"30%",cursor:"pointer"}} src={MenuButton}/>
    </form>
  </div>
</nav>
       
   
    

  
        </div>
    )
}
