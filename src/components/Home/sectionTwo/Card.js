import React from 'react'
import './Card.css'
export default function Card() {
    return (
        <div className="row mt-2 m-0 p-4">
            <div className="col-md-4">
                <div className="card-style">
                    <b >Grow Your Startup</b>
                    <p className="p-2">Whether your startup needs an HQ, funding, or prospective clients, RocketSpace can help your company grow.</p>
                    <button type="button " class=" card-btn-style btn btn-primary btn-lg">Learn How</button>
                </div>
            </div>
            <div className="col-md-4">
            <div className="card-style">
                    <b >Fuel Corporate Innovation</b>
                    <p className="p-2">RocketSpace brings together corporate innovators and leading technology startups to transform industries globally.</p>
                 <button type="button " class=" card-btn-style btn btn-primary btn-lg">GET CONNECTED</button>
                </div>
            </div>
            <div className="col-md-4">
            <div className="card-style">
                    <b >Accelerate With Purpose</b>
                    <p className="p-2">Fuel innovation through Industry Collaborative acceleration programs with dedicated cohorts of corporate industry leaders.</p>
                 <button type="button " class=" card-btn-style btn btn-primary btn-lg">LEARN MORE</button>
                </div>
            </div>
            
        </div>
    )
}
