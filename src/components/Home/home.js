import React from 'react';
import VideoSection from './sectionOne/videoSection';
import Cardd from './sectionTwo/Card';
import Sliderr from './sectionThree/slider'
export default function home() {
    return (
        <div>
            <VideoSection />
            <Cardd />
            <Sliderr />
        </div>
    )
}
