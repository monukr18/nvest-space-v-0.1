import React from 'react'
import './videoSection.css';
import BgImage from '../../../images/bgimage.jpg'
export default function videoSection() {
    return (
        <div class=" bg-image">
  <div class="leftt">
      <h1>Boldly go.</h1>
      </div>

  <div class="centered">
      <h1>We provide velocity to the world’s top innovators because they are the future.</h1>
      </div>

  <div class="rightt">
  <button type="button " class=" learn-btn-style btn btn-primary btn-lg">Learn More</button>
      </div>
</div>
    )
}
