import React from 'react'

export default function slider() {
    return (
        <div>
            <div className="row m-0 p-4">
                <div className="col-md-4">
                    <h4>WE’VE WORKED WITH</h4>
                    <p>Over 2,000 high-growth tech startups and 170 corporate brands from around the globe trust RocketSpace to steward their innovation journey. </p>
                </div>
                <div className="col-md-8">
                    
                <marquee  direction="left" >
                    <div className="row m-0">
                        <div className="col-md-3">Nvest</div>
                        <div className="col-md-3">Google</div>
                        <div className="col-md-3">Facebook</div>
                        <div className="col-md-3">Microsoft</div>
                    </div>
                </marquee>

                <marquee  direction="left" >
                    <div className="row m-0">
                        <div className="col-md-3">Microsoft</div>
                        <div className="col-md-3">Facebook</div>
                        <div className="col-md-3">Google</div>
                        <div className="col-md-3">Nvest</div>
                    </div>
                </marquee>

                <marquee  direction="left" >
                    <div className="row m-0">
                        <div className="col-md-3">Nvest</div>
                        <div className="col-md-3">Google</div>
                        <div className="col-md-3">Facebook</div>
                        <div className="col-md-3">Microsoft</div>
                    </div>
                </marquee>

                <marquee  direction="left" >
                    <div className="row m-0">
                        <div className="col-md-3">Microsoft</div>
                        <div className="col-md-3">Facebook</div>
                        <div className="col-md-3">Google</div>
                        <div className="col-md-3">Nvest</div>
                    </div>
                </marquee>

                <marquee  direction="left" >
                    <div className="row m-0">
                        <div className="col-md-3">Nvest</div>
                        <div className="col-md-3">Google</div>
                        <div className="col-md-3">Facebook</div>
                        <div className="col-md-3">Microsoft</div>
                    </div>
                </marquee>

                </div>
            </div>
        </div>
    )
}
