import React from 'react';
import './footer.css';
import Rocket from '../../images/startup.svg'
export default function footer() {
    return (
        <div className="footer">
            <div className="row m-0">
          <div className=" col-7">
          <div className="address pt-4">
           <p>Floor 14, 535 Mission Street, San Francisco, CA 94105  /
            <a class="tel" style={{color:"white"}} href="tel:+1%20415-483-1202"><strong>+44 20-3865-5879</strong> </a>
            </p>
           </div>
           <div className="menu">
               <ul className="ulstl">
                   <strong>
                   <li>Home</li>
                   <li>Events</li>
                   <li>Technology Campus</li>
                   <li>Corporate Innovation Services</li>
                   <li>Startup Members + Alumni</li>
                   <li>Corporate Clients</li>
                   <li>About</li>
                   <li>FAQ</li>
                   <li>Partners</li>
                   <li>Careers</li>
                   <li>News</li>
                   <li>London Event Space</li>
                   <li>Blog</li>
                   <li>Contact</li>
                   </strong>
               </ul>
           </div>
           </div>
          </div>
          <div className="row m-0 pt-5">
              <div className="col-md-4">
                  <span>Copyright 2011-2019 RocketSpace, Inc. <b>Privacy Policy</b></span>
              </div>
              <div className="col-md-3" style={{textAlign:"center"}}>
                  <img style={{width:"8%"}} src={Rocket}/>
              </div>
              <div className="col-md-5">
                  <div className="row m-0">
                      <div className="col-md-3">
                          <span><i class="fab fa-facebook-f"></i> Facebook</span>
                      </div>
                      <div className="col-md-3">
                          <span><i class="fab fa-youtube"></i> YouTube</span>
                      </div>
                      <div className="col-md-3">
                          <span><i class="fab fa-twitter"></i> Twitter</span>
                      </div>
                      <div className="col-md-3">
                          <span><i class="fab fa-linkedin-in"></i> Linkedin</span>
                      </div>
                  </div>

              </div>
          </div>
        </div>
    )
}
