import React, { Component } from 'react';
import './testimonial.css';
import One from '../../../images/testimonial/one.jpg';
import Two from '../../../images/testimonial/two.jpg';
import Three from '../../../images/testimonial/three.jpg';
import Four from '../../../images/testimonial/four.jpg';
import Five from '../../../images/testimonial/five.jpg';
export default class testimonial extends Component {
    state={
        toggle1:true,
        toggle2:false,
        toggle3:false,
        toggle4:false,
        toggle5:false
    }
    handleOne = () =>{
        this.setState({
        toggle1:true,
        toggle2:false,
        toggle3:false,
        toggle4:false,
        toggle5:false
        })
    }
    
    handleTwo = () =>{
        this.setState({
        toggle1:false,
        toggle2:true,
        toggle3:false,
        toggle4:false,
        toggle5:false
        })
    }
    handleThree = () =>{
        this.setState({
        toggle1:false,
        toggle2:false,
        toggle3:true,
        toggle4:false,
        toggle5:false
        })
    }
    handleFour = () =>{
        this.setState({
        toggle1:false,
        toggle2:false,
        toggle3:false,
        toggle4:true,
        toggle5:false
        })
    }
    handleFive = () =>{
        this.setState({
        toggle1:false,
        toggle2:false,
        toggle3:false,
        toggle4:false,
        toggle5:true
        })
    }

    render() {
        return (
            <div className="test-home-style">
                <div className="row m-0">
                    <div className="col-12" style={{display:"flex",justifyContent:"center"}}>
                        <div className="test-style">
                            <div>
                                <img className={this.state.toggle1?"test-img-style op-uns":"test-img-style op-stl"} style={{cursor:"pointer"}} onClick={this.handleOne} src={One} />
                            </div>
                            <div>
                                <img className={this.state.toggle2?"test-img-style op-uns":"test-img-style op-stl"} style={{cursor:"pointer"}} onClick={this.handleTwo} src={Two} />
                            </div>
                            <div>
                                <img className={this.state.toggle3?"test-img-style op-uns":"test-img-style op-stl"} style={{cursor:"pointer"}} onClick={this.handleThree} src={Three} />
                            </div>
                            <div>
                                <img className={this.state.toggle4?"test-img-style op-uns":"test-img-style op-stl"} style={{cursor:"pointer"}} onClick={this.handleFour} src={Four} />
                            </div>
                            <div>
                                <img className={this.state.toggle5?"test-img-style op-uns":"test-img-style op-stl"} style={{cursor:"pointer"}} onClick={this.handleFive} src={Five} />
                            </div>
                        </div> 
                    </div>
                </div>
                {this.state.toggle1 ?
                <div className="row m-0">
                <div className="col-12 test-list-cont">
                <h6><strong>Cory Jones</strong> VP Marketing, Skyroam</h6>
                <p style={{fontSize:"24px",lineHeight:"45px"}}>
                "RocketSpace is efficient, flexible and central. It couldn't be easier to get setup and grow. 
                And it has such an amazing location, which is fantastic as we build our team and community... 
                The facilities and the team have really been amazing partners in our growth”.
                </p>
                </div>
            </div> 
                :""}
                {this.state.toggle2 ?
                <div className="row m-0">
                <div className="col-12 test-list-cont">
                <h6><strong>Maci Peterson</strong> Co-founder + CEO, On Second Thought</h6>
                <p style={{fontSize:"24px",lineHeight:"45px"}}>
                "We chose RocketSpace because of its track record of accelerating successful companies including alumni Uber, 
                Spotify and Weebly. What we've appreciated most are the introductions to corporate executives from large players in our space. 
                In the past few months, 
                I've met leadership from PWC, IBM, and State Farm who have become useful resources thanks to RocketSpace."
                </p>
                </div>
            </div> 
                :""}
                {this.state.toggle3 ?
                <div className="row m-0">
                <div className="col-12 test-list-cont">
                <h6><strong>Kevin Mannion</strong> VP Marketing, Datahug</h6>
                <p style={{fontSize:"24px",lineHeight:"45px"}}>
                “For us, it was really important to have a flexible, hassle-free, professional place to work from. 
                At RocketSpace, we have been able to create a space of our own, build a strong company culture, 
                grow our team from two to fourteen people and double the size of our business in a year and a half. 
                The managed office space allows us to focus on activities that really move the needle for our company. 
                We never have to worry about the phones or WiFi not working, and we're proud to invite clients and investors to visit us at RocketSpace. 
                Plus, we love the community and happy hours. 
                We have made some great connections there that led to new customers!”
                </p>
                </div>
            </div> 
                :""}
                {this.state.toggle4 ?
                <div className="row m-0">
                <div className="col-12 test-list-cont">
                <h6><strong>Todd Mostak</strong> Co-founder + CEO, MapD</h6>
                <p style={{fontSize:"24px",lineHeight:"45px"}}>
                “RocketSpace is the perfect environment for startups in that intermediate zone to grow into mature companies. 
                When we first joined, we were 4 people. Now we’re a team of 23, have raised a $10 Million Series A round and launched our product. 
                At RocketSpace, you have this tremendous resource in other founders who are going through the same things you are. 
                You also have access to talks with really impressive people such as the CMO of Slack or the founder of Trulia, 
                and exposure to all of these potential clients through RocketSpace’s Corporate Innovation Services. 
                The infrastructure is great too. The team is super friendly and was so flexible in helping us expand into larger offices as our team grew. 
                RocketSpace just feels like home for us.”
                </p>
                </div>
            </div> 
                :""}
                {this.state.toggle5 ?
                <div className="row m-0">
                <div className="col-12 test-list-cont">
                <h6><strong>Dane Matheson</strong> Head of Growth Strategy, Appster</h6>
                <p style={{fontSize:"24px",lineHeight:"45px"}}>
                “An amazing place to work and meet likeminded people who want to change the world in some way or another. 
                The staff are amazing and are always willing to go above and beyond to make your journey pleasurable.”
                </p>
                </div>
            </div> 
                :""}
            </div>
        )
    }
}
