import React from 'react';
import './SectionFour.css'
export default function SectionFour() {
    return (
        <div className="tech-sec-four-bg">
           
           <div className="header-sec-two">
                <h3>Our Unique Ecosystem <strong>Helps Tech Startups Grow</strong> </h3>
            </div>
            <div className="stats-cards"> 
            <div className="box-stl">
                <div className="box-content">
                    <h1>1.5</h1>
                    <span>startups secure funding per week</span>
                </div>
            </div>
            <div className="box-stl stat-pink">
            <div className="box-content">
                    <h1>18</h1>
                    <span>alumni with $1b+ valuations</span>
                </div>
            </div>
            <div className="box-stl stat-pink">
            <div className="box-content">
                    <h1>125+</h1>
                    <span>corporate partners</span>
                </div>
            </div>
           </div>
        </div>
    )
}
