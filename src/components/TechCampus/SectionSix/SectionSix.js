import React from 'react'
import './SectionSix.css'
export default function SectionSix() {
    return (
        <div className="tec-sec-home">
            <div className="tech-sec-six-bg">
            <div className="tech-sec-six-bg-content">
                <div className="tech-sec-content-head-six">
                THE BEST EVENT VENUE IN LONDON
                </div>
                <h3 className="tech-sec-content-head-six-content">
                HOST YOUR EVENT AT ROCKETSPACE LONDON
                </h3>
                <div className="style-button-six">
                <button type="button " class="btn btn-primary btn-lg tech-btn-stl-six">REQUEST INFO</button>
                </div>
            </div>
        </div>
        </div>
        
    )
}
