import React from 'react';
import './SectionTwo.css'
import Slider1 from '../../../images/slider1.png'
export default function SectionTwo() {
    return (
        <div className="tech-sec-two-bg">

            <div className="header-sec-two">
                <h3>Amazing Tech Startups <strong>Begin At RocketSpace</strong></h3>
            </div>

            <div>
            <img style={{width:"100%"}} src={Slider1}/>
            </div>
            
        </div>
    )
}
