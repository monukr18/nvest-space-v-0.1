import React from 'react'
import './SectionFive.css';
import PrivateOffice from '../../../images/private_office.jpg';
import DedicatedDesk from '../../../images/dedicated_desk.jpg';
import SurfDesk from '../../../images/surf_desk.jpg';
export default function SectionFive() {
    return (
        <div className="tech-sec-five-bg">
            <div className="container">
            <div class="card-group">
  <div class="card" >
    <img src={PrivateOffice} class="card-img-top" alt="..."/>
    <div class="card-body">
      <h2 >Private Office</h2>
      <h6 style={{color:"#454545"}}>Rise-fall desks</h6>
      <h6 style={{color:"#454545"}}>Whiteboard walls</h6>
      <h6 style={{color:"#454545"}}>In-suite storage</h6>
      <h6 style={{color:"#454545"}}>Double-pane glass for acoustic privacy</h6>
      <h6 style={{color:"#454545"}}>Adjustable lighting, heating, and cooling</h6>
    </div>
  </div>
  <div class="card">
    <img src={DedicatedDesk} class="card-img-top" alt="..."/>
    <div class="card-body">
      <h2 >Dedicated Desk</h2>
      <h6 style={{color:"#454545"}}>Premium seating</h6>
      <h6 style={{color:"#454545"}}>Rise-fall desks</h6>
      <h6 style={{color:"#454545"}}>Space available for branding</h6>
    </div>
  </div>
  <div class="card">
    <img src={SurfDesk} class="card-img-top" alt="..."/>
    <div class="card-body">
      <h2 >Surf Desk</h2>
      <h6 style={{color:"#454545"}}>Premium seating</h6>
      <h6 >Access to all campus amenities 10 days per month</h6>
    </div>
  </div>
</div>
                </div>
        </div>
    )
}
