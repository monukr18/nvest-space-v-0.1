import React from 'react'
import './location.css'
export default function location() {
    return (
        <div>
            <div className="tech-home-loc-style">
            <div className="row m-0" style={{boxShadow:"10px 30px 60px 0 rgba(0,0,0,.14)"}}>
                <div className="col-md-6 col-sm-12 col-xs-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.7748100459235!2d77.58361781448345!3d12.922190019457659!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae15618cc6444f%3A0x7a633a2e9a181d3!2sNvest%20Campus!5e0!3m2!1sen!2sin!4v1572026195823!5m2!1sen!2sin" style={{width:"600px",height:"450px",border:"0"}} frameborder="0"  allowfullscreen=""></iframe>
                </div>
                <div className="col-md-6 col-sm-12 col-xs-12">
                   <div style={{textAlign:"center",paddingTop:"9em"}}>
                   <h3>Nvest Campus</h3>
                    <h4>Jayanagar, Bengaluru, Karnataka 560041</h4>
                    <h4>+91-12345678</h4>
                   </div>
                </div>
            </div>
        </div>
        <div className="container">
            <h2 style={{marginBottom:"75px",fontSize:"48px"}}>Other RocketSpace Services</h2>
        </div>
        </div>
    )
}