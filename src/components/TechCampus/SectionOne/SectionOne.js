import React from 'react';
import './SectionOne.css'
export default function SectionOne() {
    return (
        <div className="tech-sec-one-bg">
            <div className="tech-sec-one-bg-content">
                <div className="tech-sec-content-head">
                London Campus
                </div>
                <h1 className="tech-sec-content-head-content pt-4">Where London Tech Startups <br/> Change the World</h1>
                <div className="style-button">
                <button type="button " class="btn btn-primary btn-lg tech-btn-stl">REQUEST A TOUR</button>
                <button type="button " class="btn btn-primary btn-lg tech-btn-stl-play">PLAY VIDEO</button>
                </div>
            </div>
        </div>
    )
}
