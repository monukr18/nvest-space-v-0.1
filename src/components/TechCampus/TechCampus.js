import React from 'react'
import SectionOne from './SectionOne/SectionOne';
import SectionTwo from './SectionTwo/SectionTwo';
import SectionThree from './SectionThree/SectionThree';
import SectionFour from './SectionFour/SectionFour';
import SectionFive from './SectionFive/SectionFive';
import SectionSix from './SectionSix/SectionSix';
import Testimonial from './Testimonial/testimonial';
import Location from './Location/location'
export default function TechCampus() {
    return (
        <div>
            <SectionOne />
            <SectionTwo />
            {/* <SectionThree /> */}
            <SectionFour />
            <SectionFive />
            <SectionSix />
            <Testimonial />
            
            <Location />
        </div>
    )
}
